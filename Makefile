.ONESHELL:

.phony:
build:
	docker build -t ulrichschreiner/py3-rethinkdb:latest .

.phony:
push:
	docker push ulrichschreiner/py3-rethinkdb:latest

