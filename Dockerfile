FROM ubuntu

RUN apt -y update && apt -y dist-upgrade
RUN apt -y install python3 python3-pip
RUN pip3 install rethinkdb